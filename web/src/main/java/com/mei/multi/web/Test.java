package com.mei.multi.web;

import java.util.HashSet;
import java.util.Random;

public class Test {

    public static void main(String[] args) {

        Random random = new Random();
        for (int i = 0; i < 100; i++) {
            System.out.println(random.nextInt(10));
        }

        HashSet<Integer> hashSet = new HashSet<>();
        hashSet.add(1);
        hashSet.add(2);
        hashSet.add(3);
        hashSet.add(4);
        hashSet.add(5);
        System.out.println(hashSet);
        System.out.println(hashSet.contains(2));

        System.out.println(hashSet.size());

    }

}