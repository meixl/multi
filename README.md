# multi

#### 介绍
springboot 多 modules 项目, 共 3 层

#### 说明

启动类 `StartApplication`

参考资料: [https://blog.csdn.net/baidu_41885330/article/details/81875395](https://blog.csdn.net/baidu_41885330/article/details/81875395)

#### 项目结构

`entity`、`service`、`web`在同一目录，各个 java 文件所在位置相同。

`web`：java 文件、pom、启动类、配置文件、单元测试（需要的话）

`entity`、`service`：java 文件、pom、启动类

```
D:.
│  .gitignore
│  pom.xml
│  README.md
│  
│      
├─entity
│  │  pom.xml
│  │  
│  └─src
│      └─main
│          └─java
│              └─com
│                  └─mei
│                      └─multi
│                          └─entity
│                                  Dog.java
│                                  
├─service
│  │  pom.xml
│  │  
│  └─src
│      └─main
│          └─java
│              └─com
│                  └─mei
│                      └─multi
│                          └─service
│                                  DogService.java
│                                  DogServiceImpl.java
│                                  
└─web
    │  pom.xml
    │  
    └─src
        └─main
            ├─java
            │  └─com
            │      └─mei
            │          └─multi
            │              └─web
            │                      DogController.java
            │                      StartApplication.java
            │                      
            └─resources
                │  application.properties
                │  
                ├─static
                └─templates
```


#### 打包

父项目打包，整个项目进行打包更新即可，就是慢了点。

如果只是对外接口打包，只需打包部分即可。

实际还是建议父项目打包，总可能改了里面的代码。

打包更新，应该由工具定时更新而非人力操作。

#### 生成结构树

win tree 命令

生成目录（/a）、目录及文件（/f）到 dos、同目录指定文件。命令如下：

* tree /a
* tree /a > list.txt
* tree /f
* tree /f > list.txt

markdown 中，tree 结构代码请用 ``` 包一下，不然无法正常显示 tree 结构。