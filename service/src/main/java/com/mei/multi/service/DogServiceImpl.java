package com.mei.multi.service;

import java.time.LocalDateTime;
import java.util.Random;

import org.springframework.stereotype.Service;

import com.mei.multi.entity.Dog;

@Service
public class DogServiceImpl implements DogService {

    @Override
    public Dog get() {
        return new Dog().setColor("红色").setId(new Random().nextInt(1000)).setName(LocalDateTime.now().toString()).setGender("公的");
    }

}