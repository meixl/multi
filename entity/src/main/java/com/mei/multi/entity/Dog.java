package com.mei.multi.entity;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class Dog {

    private Integer id;

    private String  name;

    private String  color;

    private String  gender;

}