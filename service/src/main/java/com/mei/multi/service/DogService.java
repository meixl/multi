package com.mei.multi.service;

import com.mei.multi.entity.Dog;

public interface DogService {

    Dog get();

}