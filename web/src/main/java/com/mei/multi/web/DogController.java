package com.mei.multi.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mei.multi.entity.Dog;
import com.mei.multi.service.DogService;

@RestController
@RequestMapping("/dog")
public class DogController {

    @Autowired
    private DogService dogService;

    @RequestMapping("/get")
    public Dog get() {
        return dogService.get();
    }

}